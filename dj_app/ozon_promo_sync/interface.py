from .models import Shop, Report
from typing import Union


class PromoSyncShopControlInterface:

    def __init__(self, shop_client_id:str=None):
        self.shop_client_id = shop_client_id
        
    
    @classmethod
    def create(cls, client_id:str, api_key:str, name:str, refresh_time:int, active:bool=False, **kwargs) -> Shop:
        """
        """
        defaults = {
            "api_key": api_key,
            "name": name,
            "refresh_time": refresh_time,
            "active": active
        }
        new_shop, created = Shop.objects.get_or_create(
            client_id=client_id,
            defaults=defaults
        )
        return new_shop


    def get(self) -> Union[Shop, None]:
        shop_filter = Shop.objects.filter(client_id=self.shop_client_id)
        if len(shop_filter) == 0:
            return None
        else:
            return shop_filter[0]


    def remove(self) -> str:
        shop = self.get()
        if shop:
            shop.delete()
            return "Success"
        else:
            return "Shop with this client_id isn't exists"


    def edit(self, api_key:str=None, name:str=None, refresh_time:int=None, active:bool=None) -> Union[Shop, None]:
        shop = self.get()
        if not shop:
            return None
        else:
            if api_key:
                shop.api_key = api_key
            if name:
                shop.name = name
            if refresh_time:
                shop.refresh_time = refresh_time
            if active:
                shop.active = active
            shop.save()
            return shop


    def get_last_report(self) -> Union[Report, None]:
        shop = self.get()
        return shop.last_report