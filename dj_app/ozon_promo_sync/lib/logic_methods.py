from .ozon_requests import get_actions
from .ozon_requests import get_actions_candidates
from .ozon_requests import get_actions_products
from .ozon_requests import add_products_to_action
from .ozon_requests import remove_products_from_action
import json
from pprint import pprint


#### Utils
def get_all_items_with_pagination(function, client_id,  api_key, action_id, page_size):
    page = 0
    total = None
    items = []
    while True:
        limit, offset = page_size, page*page_size
        response = function(client_id, api_key, action_id, limit, offset)
        data = json.loads(response.text)
        total = data["result"]["total"]
        items += data["result"]["products"]
        if total <= len(items):
            break
        page += 1
    return items

def array_division(array:list, division_by:int) -> list:
    """ Функция делит один большой список на несколькно малениких по division_by элементов в каждом """
    count = len(array)//division_by + 1
    final_list = []
    start:int = 0
    finish:int = 1
    while finish <= count:
        final_list.append(array[start*division_by : finish*division_by])
        start += 1
        finish += 1
    return final_list
###


def carry_over_products(client_id, api_key, action_id, get_function, carry_function, to_action=True):
    """
    Pseudocode:
    for action_candidate in action_candidates:
        if action_candidate.price <= action_candidate.max_action_price
            action_candidate.add_to_action
    """
    output = []
    add_to_action_products = []
    try:
        action_candidates = get_all_items_with_pagination(get_function, client_id, api_key, action_id, 1000)
    except:
        return output

    for candidate in action_candidates:
        if to_action:
            if candidate["price"] <= candidate["max_action_price"]:
                add_to_action_products.append({
                    "product_id": candidate["id"],
                    "action_price": candidate["price"]
                })
        else:
            if candidate["price"] > candidate["max_action_price"]:
                add_to_action_products.append(candidate["id"])
    if len(add_to_action_products) == 0:
        return output
    chunks = array_division(add_to_action_products, 1000)
    for chunk in chunks:
        response = carry_function(client_id, api_key, action_id, chunk)
        products = json.loads(response.text)["result"]["product_ids"]
        output += products
    return output


def check_all_actions_and_carry_over_products(client_id, api_key, to_action=True):
    """
    Pseudocode:
    for action in actions:
        set_action_to_action_candidates
    """
    response = get_actions(client_id, api_key)
    actions_list = json.loads(response.text)["result"]

    for action in actions_list:
        if action["action_type"] != "DISCOUNT":
            continue
        if to_action:
            carrying_products = carry_over_products(client_id, api_key, action["id"], get_actions_candidates, add_products_to_action, to_action)
        else:
            carrying_products = carry_over_products(client_id, api_key, action["id"], get_actions_products, remove_products_from_action, to_action)
        yield {
            "action_id": action["id"],
            "products": carrying_products
        }