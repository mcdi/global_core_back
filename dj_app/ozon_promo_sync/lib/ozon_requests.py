import requests


def get_actions(client_id, api_key) -> requests.models.Response:
    """
    Function returns actual actions
    """
    url = "https://api-seller.ozon.ru/v1/actions"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key
    }
    response = requests.get(
        url=url,
        headers=headers
    )
    return response


def get_actions_candidates(client_id, api_key, action_id:int, limit:int, offset:int) -> requests.models.Response:
    url = "https://api-seller.ozon.ru/v1/actions/candidates"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key
    }
    payload = {
        "action_id": action_id,
        "limit": limit,
        "offset": offset
    }
    response = requests.post(
        url=url,
        headers=headers,
        json=payload
    )
    return response


def get_actions_products(client_id, api_key, action_id:int, limit:int, offset:int) -> requests.models.Response:
    url = "https://api-seller.ozon.ru/v1/actions/products"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key
    }
    payload = {
        "action_id": action_id,
        "limit": limit,
        "offset": offset
    }
    response = requests.post(
        url=url,
        headers=headers,
        json=payload
    )
    return response


def add_products_to_action(client_id:str, api_key:str, action_id:int, products:list):
    """
    Products must be in format: [{"product_id": "***", "action_price": 931.00}, ...]
    """
    url = "https://api-seller.ozon.ru/v1/actions/products/activate"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key
    }
    payload = {
        "action_id": action_id,
        "products": products
    }
    response = requests.post(
        url=url,
        headers=headers,
        json=payload
    )
    return response


def remove_products_from_action(client_id:str, api_key:str, action_id:int, product_ids:list):
    """
    Products must be in format: ["***", "***", "***", "***", ...]
    """
    url = "https://api-seller.ozon.ru/v1/actions/products/deactivate"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key
    }
    payload = {
        "action_id": action_id,
        "product_ids": product_ids
    }
    response = requests.post(
        url=url,
        headers=headers,
        json=payload
    )
    return response