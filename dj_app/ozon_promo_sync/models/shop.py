from django.db import models
from django.utils import timezone


class Shop(models.Model):
    """

    """
    name = models.CharField(max_length=1024)
    client_id = models.CharField(max_length=20)
    api_key = models.CharField(max_length=50)

    refresh_time = models.IntegerField(default=0)
    active = models.BooleanField(default=False)

    @property
    def last_report(self):
        return self.report_set.last()

    @property
    def is_refresh_time(self) -> bool:
        if not self.active:
            return False
        if not  self.last_report:
            return True
        now = timezone.now()
        delta = now - self.last_report.created_at
        return delta.total_seconds() // 60 > self.refresh_time