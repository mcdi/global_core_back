from django.db import models
from .shop import Shop



class Report(models.Model):
    """

    """
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    started_at = models.DateTimeField(null=True, blank=True)
    finished_at = models.DateTimeField(null=True, blank=True)
    success = models.BooleanField(default=False)
    detail = models.JSONField(null=True, blank=True)