from rest_framework import serializers
from ..models import Report


class ReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = Report
        fields = ['id', 'shop', 'created_at', 'updated_at', 'started_at', 'finished_at', 'success', 'detail']