from rest_framework.routers import SimpleRouter
from .views import ShopViewSet


router = SimpleRouter()

router.register("shop", ShopViewSet, basename="Ozon Promo Sync Shop")

urlpatterns = router.urls