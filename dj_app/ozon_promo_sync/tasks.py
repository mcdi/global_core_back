from celery import shared_task
from .models import Shop, Report
from .lib.logic_methods import check_all_actions_and_carry_over_products
from django.utils import timezone
import traceback


@shared_task
def sync_one_shop(shop_id: int) -> None:
    shop = Shop.objects.get(id=shop_id)

    new_report = Report.objects.create(shop_id=shop_id)
    new_report.started_at = timezone.now()
    new_report.save()

    report_detail = {
        "to_action": {},
        "to_normal": {}
    }
    try:
        # To Action
        to_action_generator = check_all_actions_and_carry_over_products(shop.client_id, shop.api_key, to_action=True)
        for action_response in to_action_generator:
            report_detail["to_action"][action_response["action_id"]] = len(action_response["products"])
            new_report.detail = report_detail
            new_report.save()
        # To Normal
        to_normal_generator = check_all_actions_and_carry_over_products(shop.client_id, shop.api_key, to_action=False)
        for action_response in to_normal_generator:
            report_detail["to_normal"][action_response["action_id"]] = len(action_response["products"])
            new_report.detail = report_detail
            new_report.save()
        new_report.success = True
    except:
        new_report.detail = {
            "traceback": traceback.format_exc()
        }
        new_report.save()

    new_report.finished_at = timezone.now()
    new_report.save()


@shared_task
def check_all_shops() -> None:
    shops = Shop.objects.all()
    for shop in shops:
        if shop.is_refresh_time:
            sync_one_shop.delay(shop.id)