from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework.viewsets import ViewSet
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Shop
from .serializers import ShopSerializer, ReportSerializer
from rest_framework.permissions import AllowAny
from .interface import PromoSyncShopControlInterface


class ShopViewSet(ViewSet):

    queryset = Shop.objects.all()
    serializer_class = ShopSerializer
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        responses={
            200: openapi.Response('Shops list', ShopSerializer(many=True)),
        }, tags=['Ozon Promo Sync'])
    @action(methods=["GET"], detail=False)
    def all(self, request):
        shops = Shop.objects.all()
        return Response({"result": ShopSerializer(shops, many=True).data})

    @swagger_auto_schema(
    manual_parameters=[
        openapi.Parameter('client_id', openapi.IN_QUERY, "12345", type=openapi.TYPE_NUMBER, required=True),
        openapi.Parameter('name', openapi.IN_QUERY, "Name", type=openapi.TYPE_NUMBER, required=True),
        openapi.Parameter('api_key', openapi.IN_QUERY, "Api Key from own cabinet", type=openapi.TYPE_NUMBER, required=True),
        openapi.Parameter('refresh_time', openapi.IN_QUERY, "In minutes", type=openapi.TYPE_INTEGER, required=True),
        openapi.Parameter('active', openapi.IN_QUERY, "True or False", type=openapi.TYPE_BOOLEAN, required=True)
    ],
    responses={
        200: openapi.Response('Shops list', ShopSerializer),
    }, tags=['Ozon Promo Sync'])
    @action(methods=["POST"], detail=False)
    def add(self, request):
        new_shop_model = PromoSyncShopControlInterface.create(
            **request.data
        )
        return Response({"result": ShopSerializer(new_shop_model).data})

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('client_id', openapi.IN_QUERY, "12345", type=openapi.TYPE_NUMBER, required=True),
        openapi.Parameter('name', openapi.IN_QUERY, "Name", type=openapi.TYPE_NUMBER, required=False),
        openapi.Parameter('api_key', openapi.IN_QUERY, "Api Key from own cabinet", type=openapi.TYPE_NUMBER, required=False),
        openapi.Parameter('refresh_time', openapi.IN_QUERY, "In minutes", type=openapi.TYPE_INTEGER, required=False),
        openapi.Parameter('active', openapi.IN_QUERY, "True or False", type=openapi.TYPE_BOOLEAN, required=False),
    ], responses={
        200: openapi.Response('Edit shop configuration', ShopSerializer),
    }, tags=['Ozon Promo Sync'])
    @action(methods=["POST"], detail=False)
    def edit(self, request):
        client_id = request.data["client_id"]
        interface = PromoSyncShopControlInterface(client_id)
        shop_model = interface.create(**request.data)
        return Response({"result": ShopSerializer(shop_model).data})

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('client_id', openapi.IN_QUERY, "12345", type=openapi.TYPE_NUMBER),
    ], responses={
        200: openapi.Response('Remove shop'),
    }, tags=['Ozon Promo Sync'])
    @action(methods=["POST"], detail=False)
    def remove(self, request):
        client_id = request.data["client_id"]
        interface = PromoSyncShopControlInterface(client_id)
        return Response({"result": interface.remove()})

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('client_id', openapi.IN_QUERY, "12345", type=openapi.TYPE_NUMBER),
    ], responses={
        200: openapi.Response('Last report data', ReportSerializer),
    }, tags=['Ozon Promo Sync'])
    @action(methods=["POST"], detail=False)
    def last_report(self, request):
        client_id = request.data["client_id"]
        interface = PromoSyncShopControlInterface(client_id)
        return Response({"result": ReportSerializer(interface.get_last_report()).data})