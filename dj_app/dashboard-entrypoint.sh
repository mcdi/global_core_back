#!/bin/sh

export FLOWER_PORT=5555
export FLOWER_BROKER=$BROKER_URL

flower -A core --port=$FLOWER_PORT --broker=$FLOWER_BROKER

exec "$@"
