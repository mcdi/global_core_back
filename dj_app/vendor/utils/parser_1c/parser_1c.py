import os
from django.utils import timezone
from lxml import etree


def prepare_file(file):
    with open(file, 'rt') as f:
        xml = f.read()
    parts = xml.partition(
        'xmlns="urn:1C.ru:commerceml_2" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"')
    xml = ''.join([parts[0], parts[-1]])

    with open(file, 'wt') as f:
        f.write(xml)


def parse_1c_from_dir(dir_with_files: str):
    files = [os.path.join(dir_with_files, f) for f in os.listdir(dir_with_files) if
             os.path.isfile(os.path.join(dir_with_files, f)) and f.endswith('.xml')]
    main_file = stock_file = None
    for file in files:
        if 'import' in file:
            main_file = file
        elif 'offers' in file:
            stock_file = file

    if main_file is None or stock_file is None:
        return None

    products_by_id = {}
    prepare_file(main_file)
    main_root = etree.parse(main_file).getroot()

    date = timezone.datetime.strptime(main_root.get('ДатаФормирования'), '%Y-%m-%dT%H:%M:%S')
    catalog = {'date': date}

    offers = main_root.findall('.//Товары/')
    for offer in offers:
        products_dict = {
            'offer_id': offer.find('Артикул').text,
            'name': offer.find('Наименование').text,
        }
        for el in offer.findall('./СтавкиНалогов/'):
            if el.find('Наименование').text == 'НДС':
                vat = el.find('Ставка').text
                try:
                    products_dict['vat'] = int(vat)
                except ValueError:
                    products_dict['vat'] = 0

        for el in offer.findall('./ЗначенияРеквизитов/'):
            if el.find('Наименование').text == 'Код':
                products_dict['barcode'] = el.find('Значение').text
                break

        products_by_id[offer.find('Ид').text] = products_dict

    prepare_file(stock_file)
    stock_root = etree.parse(stock_file).getroot()
    offers = stock_root.findall('.//Предложения/')
    for offer in offers:
        curr_id = offer.find('Ид').text
        if curr_id in products_by_id:
            products_by_id[curr_id]['stock'] = offer.find('Количество').text

    catalog['products'] = list(products_by_id.values())
    for file in files:
        os.remove(file)
    return catalog


def parse_1c_from_files(main_file: str, stock_file: str):
    products_by_id = {}
    prepare_file(main_file)
    main_root = etree.parse(main_file).getroot()

    date = timezone.datetime.strptime(main_root.get('ДатаФормирования'), '%Y-%m-%dT%H:%M:%S')
    catalog = {'date': date}

    offers = main_root.findall('.//Товары/')
    for offer in offers:
        products_dict = {
            'offer_id': offer.find('Артикул').text,
            'name': offer.find('Наименование').text,
        }
        for el in offer.findall('./СтавкиНалогов/'):
            if el.find('Наименование').text == 'НДС':
                vat = el.find('Ставка').text
                try:
                    products_dict['vat'] = int(vat)
                except ValueError:
                    products_dict['vat'] = 0

        for el in offer.findall('./ЗначенияРеквизитов/'):
            if el.find('Наименование').text == 'Код':
                products_dict['barcode'] = el.find('Значение').text
                break

        products_by_id[offer.find('Ид').text] = products_dict

    prepare_file(stock_file)
    stock_root = etree.parse(stock_file).getroot()
    offers = stock_root.findall('.//Предложения/')
    for offer in offers:
        curr_id = offer.find('Ид').text
        if curr_id in products_by_id:
            products_by_id[curr_id]['stock'] = offer.find('Количество').text

    catalog['products'] = list(products_by_id.values())
    return catalog
