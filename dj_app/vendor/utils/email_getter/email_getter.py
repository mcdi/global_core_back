from imaplib import IMAP4_SSL
import email
import email.header
import email.utils
from django.utils import timezone
import urllib.request
from bs4 import BeautifulSoup
import tempfile
import os


def get_email_data(sender, host='imap.yandex.ru', port=993, user='b2b.ecom@unekon.com', password='2295489b',
                   content_type_filter='application', file_name_filter=None, forwarded_by_filter=None):
    connection = IMAP4_SSL(host=host, port=port)
    connection.login(user=user, password=password)
    status, messages = connection.select('INBOX')
    assert status == 'OK'

    files = []
    date = None
    break_flag = False
    if type(sender) is list:
        header = '(OR (FROM {}) (FROM {}))'.format(sender[0], sender[1])
        for sen in sender[2:]:
            header = '(OR (FROM {}) {}'.format(sen, header)
        typ, data = connection.search(None, header)
    else:
        if forwarded_by_filter is not None:
            header = '(OR (FROM {}) (FROM {}))'.format(sender, forwarded_by_filter)
            typ, data = connection.search(None, header)
        else:
            typ, data = connection.search(None, 'FROM {}'.format(sender))
    mails = data[0].split()
    mails.reverse()
    if typ == 'OK':
        for num in mails:
            if break_flag:
                break
            typ, message_data = connection.fetch(num, '(RFC822)')
            if typ == 'OK':
                try:
                    mail = email.message_from_string(message_data[0][1])
                except TypeError:
                    mail = email.message_from_bytes(message_data[0][1])
                date = timezone.datetime.strptime(mail['Date'][:-6], "%a, %d %b %Y %H:%M:%S")
                if mail.is_multipart():
                    for part in mail.walk():
                        forwarded_flag = False
                        part_content_type = part.get_content_type()
                        if 'text' in part_content_type and forwarded_by_filter is not None:
                            msg_text = email.message_from_bytes(part.get_payload(decode=True)).as_bytes().decode(
                                'utf-8')
                            if msg_text.find(sender) != -1:
                                forwarded_flag = True
                        if ('application' in part_content_type or 'xml' in part_content_type) \
                                and content_type_filter == 'application':
                            filename = part.get_filename()
                            filename = str(email.header.make_header(email.header.decode_header(filename)))
                            if file_name_filter is not None:
                                if not (filename.startswith(file_name_filter.title())
                                        or filename.startswith(file_name_filter.lower()) or filename.startswith(
                                            file_name_filter)):
                                    break
                                else:
                                    break_flag = True
                            elif forwarded_by_filter is not None and mail['From'] == forwarded_by_filter:
                                if forwarded_flag:
                                    break_flag = True
                            else:
                                break_flag = True
                            if filename:
                                with tempfile.NamedTemporaryFile('ab', suffix='.' + filename.split('.')[-1],
                                                                 delete=False) as f:
                                    f.write(part.get_payload(decode=True))
                                    files.append(f.name)
                        if 'html' in part_content_type and content_type_filter == 'html':
                            break_flag = True
                            page = BeautifulSoup(part.get_payload(decode=True), 'lxml')
                            for button in page.find_all('a', class_='mailbtn'):
                                if button.find('span').text == 'Скачать прайс-лист':
                                    href = button.get('href')
                            response = urllib.request.urlopen(href)
                            content = response.read()
                            with tempfile.NamedTemporaryFile('ab', suffix='.xls', delete=False) as f:
                                f.write(content)
                                files.append(f.name)

    connection.close()
    connection.logout()
    return {'date': date, 'files': files}
