import openpyxl
import pyexcel
from typing import List, Tuple, Dict, Callable
import os
from html import unescape


def parse_xl(file: str, fields: Dict[str, str], fields_rows: Tuple[int, int] = None, not_null_field: str = None,
             validators: Dict[str, Callable] = None) -> List[dict]:
    if file.endswith('.xls'):
        return parse_xls(file, fields, fields_rows, not_null_field, validators)
    elif file.endswith('.xlsx'):
        return parse_xlsx(file, fields, fields_rows, not_null_field, validators)
    else:
        raise Exception('File is not .xl file')


def parse_xls(file: str, fields: Dict[str, str], fields_rows: Tuple[int, int] = None, not_null_field: str = None,
              validators: Dict[str, Callable] = None) -> List[dict]:
    xlsx_file = file + 'x'

    pyexcel.save_book_as(
        file_name=file,
        dest_file_name=xlsx_file)

    products = parse_xlsx(xlsx_file, fields, fields_rows, not_null_field, validators)
    os.remove(xlsx_file)

    return products


def parse_xlsx(file: str, fields: Dict[str, str], fields_rows: Tuple[int, int] = None, not_null_field: str = None,
               validators: Dict[str, Callable] = None) -> List[dict]:
    fields_rows = list(fields_rows)

    book = openpyxl.load_workbook(
        filename=file,
        read_only=True
    )
    sheet = book.active

    if fields_rows is not None:
        params = {'min_row': fields_rows[0],
                  'max_row': fields_rows[-1]}
    else:
        params = {'min_row': 0,
                  'max_row': 30}
        fields_rows = [0]

    column_names = {}
    for row in sheet.iter_rows(**params):
        for cell in row:
            if cell.value is None:
                continue
            else:
                for key, value in fields.items():
                    if cell.value == value:
                        fields_rows[-1] = cell.row
                        column_names[cell.column] = key

    params = {'min_row': fields_rows[-1] + 1}
    if validators is None:
        validators = {}

    products = []
    for row in sheet.iter_rows(**params):
        product_dict = {}
        for cell in row:
            try:
                if cell.column in column_names.keys():
                    if not_null_field is not None:
                        if column_names[cell.column] == not_null_field and cell.value is None:
                            product_dict = {}
                            break
                    else:
                        if cell.value is None:
                            product_dict = {}
                            break

                    if validators.get(column_names[cell.column], None) is not None:
                        validator = validators[column_names[cell.column]]
                    else:
                        validator = default_validator
                    validated_value = validator(cell.value)
                    if type(validated_value) is dict:
                        product_dict.update(validated_value)
                    else:
                        product_dict[column_names[cell.column]] = validated_value
            except AttributeError:
                pass
            except TypeError:
                pass
        if len(product_dict) != 0:
            products.append(product_dict)

    return products


def default_validator(cell_value):
    """
    Field validator MUST:
        return dict where key is field name
        and value is validated value for this field
    OR
        return just validated value for field
    """
    if type(cell_value) is str:
        cell_value = unescape(cell_value)
        while cell_value.endswith(' '):
            cell_value = cell_value[:-1]
        while cell_value.startswith(' '):
            cell_value = cell_value[1:]

    return cell_value
