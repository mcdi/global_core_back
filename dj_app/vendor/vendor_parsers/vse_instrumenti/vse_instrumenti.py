from ...utils.xl_parser import parse_xl
from ...utils.email_getter import get_email_data
import os


def vse_instrumenti(*args, **kwargs):
    email_data = get_email_data(sender='Olap@vseinstrumenti.ru')
    files = email_data.get('files')
    xlsx_file = files[-1]

    catalog = {'date': email_data.get('date')}

    fields = {
        'offer_id': 'Код для заказа',
        'price': 'ОПТ8',
        'stock': 'Остатки в МСК-Белая дача',
        'name': 'Наименование товара',
    }

    def stock_validator(cell_value):
        if cell_value is None:
            return {'stock': 0, 'vat': 20}
        else:
            return {'stock': int(cell_value), 'vat': 20}

    catalog['products'] = parse_xl(xlsx_file, fields, fields_rows=(1, 1), not_null_field='price',
                                   validators={'stock': stock_validator})

    os.remove(xlsx_file)
    return catalog
