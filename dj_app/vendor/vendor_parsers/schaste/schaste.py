import requests
from lxml import etree
from django.utils import timezone
import os
import tempfile


def schaste(url, *args, **kwargs):
    response = requests.get(url)

    with tempfile.NamedTemporaryFile() as f:
        f.write(response.content)
        root = etree.parse(f.name).getroot()

    date = timezone.datetime.strptime(root.attrib['FileDate'], '%H:%M:%S %d.%m.%Y')
    catalog = {'date': date}

    products = []
    for offer in root:
        product_dict = {'offer_id': str(offer.attrib['vendorCode']), 'name': str(offer.attrib['name'])}
        for el in offer.getchildren():
            if el.tag == 'price':
                product_dict['price'] = float(el.attrib['BaseWholePrice'])
                product_dict['rec_price'] = float(el.attrib['BaseRetailPrice'])
            elif el.tag == 'assortiment':
                for assort in el.getchildren():
                    product_dict['stock'] = int(assort.attrib['sklad'])
                    product_dict['barcode'] = str(assort.attrib['barcode'])
                    break
        products.append(product_dict)

    catalog['products'] = products
    return catalog
