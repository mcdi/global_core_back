import os
from ...utils.email_getter import get_email_data
from django.utils import timezone
from ...utils.xl_parser import parse_xlsx


def own_warehouse(*args, **kwargs):
    email_data = get_email_data(sender='sap@ecomseller.ru')
    file = email_data.get('files')[-1]

    date = '.'.join(file.split('склад ')[-1].split('.')[:-1])
    date = timezone.datetime.strptime(date, '%d.%m.%Y')

    catalog = {'date': date}
    fields = {
        'name': 'Название',
        'offer_id': 'Арт.',
        'stock': 'Кол-во',
    }

    def name_validator(cell_value):
        if cell_value is None:
            return {}
        else:
            return cell_value

    products = parse_xlsx(file, fields=fields, fields_rows=(1, 1), not_null_field='offer_id',
                          validators={'name': name_validator})

    article_tree = {}
    for product in products:
        article = product.get('offer_id')
        if article not in article_tree:
            article_tree[article] = [product]
        else:
            article_tree[article].append(product)

    processed_products = []
    for article, products_by_art in article_tree.items():
        products_tree = {}
        for product in products_by_art:
            name = product.get('name', 'none')
            if name not in products_tree:
                products_tree[name] = [product]
            else:
                products_tree[name].append(product)
        if 'none' in products_tree:
            name = None
            stock = 0
            for product in products_by_art:
                temp_name = product.get('name', None)
                if temp_name is not None:
                    name = temp_name
                stock += product.get('stock')
            processed_products.append(
                {'name': ' '.join(name.split()[:-1]), 'offer_id': article,
                 'stock': stock}) if name is not None else processed_products.append(
                {'offer_id': article, 'stock': stock})
        else:
            processed_products.extend(products_by_art)

    catalog['products'] = processed_products
    os.remove(file)
    return catalog
