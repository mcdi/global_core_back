import os
from ...utils.xl_parser import parse_xlsx
from openpyxl import load_workbook
from django.utils import timezone
import pyexcel
import requests
import tempfile

# TODO: decide how to resolve many prices
def neotren(*args, **kwargs):
    url = 'https://neotren.ru/docs/Price_Catalog/Price_Neotren.xls'
    # inn = '7724667440'

    try:
        response = requests.get(url)
    except requests.exceptions.SSLError:
        response = requests.get(url, verify=False)
    content = response.content

    with tempfile.NamedTemporaryFile(suffix='.xls') as xls_file, tempfile.NamedTemporaryFile(
            suffix='.xlsx') as xlsx_file:
        xls_file.write(content)

        pyexcel.save_book_as(
            file_name=xls_file.name,
            dest_file_name=xlsx_file.name)

        book = load_workbook(
            filename=xlsx_file.name,
            read_only=True)
        sheet = book.active

        for row in sheet.iter_rows(min_row=16, max_row=16, min_col=22, max_col=22):
            for cell in row:
                date = cell.value.split('на ')[-1]
                date = timezone.datetime.strptime(date, '%d.%m.%Y  %H:%M:%S')

        catalog = {'date': date}

        fields = {
            'offer_id': 'Номенклатура',
            'price': 'РРЦ',
            'price1': 'РРЦ по акции',
            'stock': 'Наличие МСК',
            'name': 'brutcyak@neotren.ru',
        }

        def stock_validator(cell_value):
            if cell_value == 'Ожидается':
                return 0
            elif cell_value == 'В наличии':
                return 2

        def price_validator(cell_value):
            cell_value = cell_value.replace(' ', '')
            return float(cell_value)

        catalog['products'] = parse_xlsx(xlsx_file.name, fields, fields_rows=(15, 18), not_null_field='price',
                                         validators={'stock': stock_validator, 'price': price_validator,
                                                     'price1': price_validator})
    return catalog
