import requests
from ...utils.parser_1c import parse_1c_from_files
import tempfile


def kettler(*args, **kwargs):
    main_url = 'https://www.kettlermebel.ru/_upload/1c/import0_1.xml'
    stock_url = 'https://www.kettlermebel.ru/_upload/1c/offers0_1.xml'
    # inn = '7731647519'

    with tempfile.NamedTemporaryFile(suffix='.xml') as main_file, tempfile.NamedTemporaryFile(
            suffix='.xml') as stock_file:
        resp = requests.get(main_url)
        main_file.write(resp.content)

        resp = requests.get(stock_url)
        stock_file.write(resp.content)

        catalog = parse_1c_from_files(main_file.name, stock_file.name)

    return catalog
