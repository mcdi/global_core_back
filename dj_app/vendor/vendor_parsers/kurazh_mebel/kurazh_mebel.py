import tempfile

import requests
from django.utils import timezone
from lxml import etree


def kurazh_mebel(url, *args, **kwargs):
    # url = 'https://kurazh-mebel.ru/upload/acrit.exportproplus/ym_vendor_model_OZON.xml?1596010421'
    # inn = '6230037310'

    response = requests.get(url)
    assert response.status_code == 200

    with tempfile.NamedTemporaryFile() as f:
        f.write(response.content)
        root = etree.parse(f.name).getroot()

    date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
    catalog = {'date': date}

    products = []
    offers = root.findall(".//offers/")
    for offer in offers:
        product_dict = {
            'price': float(offer.find('price').text),
            'name': offer.find('model').text,
            'stock': int(offer.get('available')),
            'offer_id': offer.get('id'),
        }
        products.append(product_dict)

    catalog['products'] = products
    return catalog
