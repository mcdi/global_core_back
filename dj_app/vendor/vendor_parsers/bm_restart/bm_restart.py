import requests
from lxml import etree
from django.utils import timezone
from html import unescape
import os
import tempfile


# TODO: decide how to resolve many prices
def bm_restart(url, *args, **kwargs):
    response = requests.get(url)

    file_name = 'bm_restart'
    with tempfile.NamedTemporaryFile() as f:
        f.write(response.content)
        root = etree.parse(f.name).getroot()

    date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
    catalog = {'price_date': date,
               'stock_date': date}
    products = []
    product_dict = {}

    offers = root.findall(".//offers/")

    for offer in offers:
        for el in offer.getchildren():
            if el.tag == 'param':
                if el.attrib['name'] == 'Оптовая (руб)':
                    product_dict['price'] = float(el.text)
                if el.attrib['name'] == 'Опт 1 (руб)':
                    product_dict['price1'] = float(el.text)
                if el.attrib['name'] == 'Опт 2 (руб)':
                    product_dict['price2'] = float(el.text)
                if el.attrib['name'] == 'Опт 3 (руб)':
                    product_dict['price3'] = float(el.text)
                if el.attrib['name'] == 'Опт 4 (руб)':
                    product_dict['price4'] = float(el.text)
                if el.attrib['name'] == 'QUANTITY':
                    product_dict['stock'] = int(el.text)
                if el.attrib['name'] == 'Артикул':
                    product_dict['offer_id'] = str(el.text)
                if el.attrib['name'] == 'CODE':
                    product_dict['barcode'] = str(el.text)
            elif el.tag == 'name':
                product_dict['name'] = unescape(str(el.text))

        # product_dict['vat'] = None
        # product_dict['rec_price'] = None
        products.append(product_dict)
        product_dict = {}

    catalog['products'] = products
    catalog['fields_descriptions'] = {
        'price_description': 'Оптовая',
        'price1_description': 'Опт 1',
        'price2_description': 'Опт 2',
        'price3_description': 'Опт 3',
        'price4_description': 'Опт 4',
        'barcode_description': 'CODE'
    }
    return catalog
