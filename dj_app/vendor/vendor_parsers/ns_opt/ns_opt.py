import os
from ...utils.xl_parser import parse_xl
import urllib.request
from django.utils import timezone
import tempfile


# NOTE: to use must comment lines 425-427 in compdoc.py
def ns_opt(url, *args, **kwargs):
    # url = 'https://nstopt.ru/upload/priceList.xls'

    response = urllib.request.urlopen(url)
    content = response.read()

    with tempfile.NamedTemporaryFile(suffix='.xls') as f:
        f.write(content)
        xls_file = f.name

        date = timezone.datetime.utcfromtimestamp(os.stat(xls_file).st_mtime)
        catalog = {'date': date}

        fields = {
            'offer_id': 'Артикул',
            'price': 'Цена, руб',
            'stock': 'Количество на складе',
            'name': 'Наименование',
            'barcode': 'Штрихкод',
        }
        catalog['products'] = parse_xl(xls_file, fields, fields_rows=(1, 1), not_null_field='price')
        os.remove(xls_file)
    return catalog
