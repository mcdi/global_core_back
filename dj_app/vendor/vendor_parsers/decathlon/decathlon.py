import requests
from django.utils import timezone
from lxml import etree
import os
import tempfile


def decathlon(url, *args, **kwargs):
    # url = 'https://master-ecomm-datafeed-service.storage.yandexcloud.net/ng_decathlon.yml'
    # inn = '5029086747'

    response = requests.get(url)
    assert response.status_code == 200

    with tempfile.NamedTemporaryFile() as f:
        f.write(response.content)
        root = etree.parse(f.name).getroot()

    date = timezone.datetime.strptime(root.get('date'), '%Y-%m-%d %H:%M')
    catalog = {'date': date}

    products = []
    offers = root.findall(".//offers/")

    for offer in offers:
        product_dict = {
            'price': float(offer.find('price').text),
            'offer_id': offer.find('vendorCode').text,
            'name': offer.find('name').text,
            'barcode': offer.find('barcode').text,
        }
        for outlet in offer.findall('./outlets/outlets'):
            product_dict['stock'] = int(outlet.get('instock'))
        products.append(product_dict)

    catalog['products'] = products
    return catalog
