import requests
from lxml import etree
from django.utils import timezone
import tempfile


def instrument_ru(url, *args, **kwargs):
    # url = 'http://new.grand-instrument.ru/bitrix/catalog_export/yandex_ozon.php'
    # inn = '540447379797'

    response = requests.get(url)
    assert response.status_code == 200

    with tempfile.NamedTemporaryFile() as f:
        f.write(response.content)
        root = etree.parse(f.name).getroot()

    date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
    catalog = {'date': date}
    products = []
    offers = root.findall(".//offers/")
    for offer in offers:
        product_dict = {
            'price': float(offer.find('price').text),
            'name': offer.find('name').text,
            'barcode': offer.get('id')
        }
        for param in offer.findall('./param'):
            if param.get('name') == 'Код для сайта':
                product_dict['offer_id'] = param.text
        for outlet in offer.findall('.//outlet'):
            if outlet.get('id') == '0':
                try:
                    product_dict['stock'] = int(outlet.get('instock'))
                except ValueError:
                    product_dict['stock'] = round(float(outlet.get('instock')))
            else:
                product_dict['stock'] = 0
        products.append(product_dict)

    catalog['products'] = products
    return catalog
