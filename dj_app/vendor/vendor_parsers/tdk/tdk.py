from openpyxl import load_workbook
import pyexcel
from ...utils.email_getter import get_email_data
import os
from django.utils import timezone
from ...utils.xl_parser import parse_xlsx


def tdk(*args, **kwargs):
    email_data = get_email_data(sender='yu.denisenko@tdkru.ru')
    files = email_data['files']

    xls_file = files[-1]
    xlsx_file = xls_file + 'x'

    pyexcel.save_book_as(
        file_name=xls_file,
        dest_file_name=xlsx_file)

    os.remove(xls_file)

    book = load_workbook(
        filename=xlsx_file,
        read_only=True)
    sheet = book.active

    catalog = {}

    for row in sheet.iter_rows(min_row=5, max_row=5, min_col=2, max_col=2):
        for cell in row:
            if 'Цены указаны' in cell.value:
                date = cell.value.split('на')[-1]
                while date.startswith(' '):
                    date = date[1:]
                date = timezone.datetime.strptime(date, '%d.%m.%Y')
                catalog = {'date': date}

    def stock_validator(cell_value):
        if cell_value is not None:
            return int(cell_value)
        else:
            return 0

    fields = {
        'offer_id': 'Код',
        'price': 'Цена',
        'stock': 'Остаток',
        'name': 'Ценовая группа/ Номенклатура/ Характеристика номенклатуры',
        'barcode': 'Штрих-код',
    }
    catalog['products'] = parse_xlsx(xlsx_file, fields, fields_rows=(8, 9), not_null_field='offer_id',
                                     validators={'stock': stock_validator})
    os.remove(xlsx_file)
    return catalog
