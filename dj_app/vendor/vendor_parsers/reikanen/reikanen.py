from ...utils.email_getter import get_email_data
from ...utils.xl_parser import parse_xl
import os


def reikanen(*args, **kwargs):
    email_data = get_email_data(sender='price@reikanen.ru')
    file = email_data.get('files')[-1]
    date = email_data.get('date')

    catalog = {'date': date}

    fields = {
        'offer_id': 'Артикул',
        'price': 'Цена',
        'stock': 'Наличие',
        'name': 'Наименование',
    }

    catalog['products'] = parse_xl(file, fields, fields_rows=(1, 1), not_null_field='offer_id')

    os.remove(file)
    return catalog
