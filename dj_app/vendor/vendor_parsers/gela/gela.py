from ftplib import FTP
import requests
from lxml import etree
import os
from django.utils import timezone
import tempfile


def gela(*args, **kwargs):
    with FTP(host='ws.gela.ru') as ftp:
        ftp.login(user='vigruzka', passwd='ahmeNe9E')
        opt_price = tempfile.NamedTemporaryFile(suffix='.xml')
        ftp.retrbinary('RETR gela_opt_price.xml', opt_price.write)
        rrc_price = tempfile.NamedTemporaryFile(suffix='.xml')
        ftp.retrbinary('RETR gela_rrc_price.xml', rrc_price.write)

    curr_req = requests.get('http://www.cbr.ru/scripts/XML_daily.asp')
    assert curr_req.status_code == 200
    with tempfile.NamedTemporaryFile(suffix='.xml') as f:
        f.write(curr_req.content)

        root = etree.parse(f.name).getroot()

        currs = root.findall("Valute")
        for curr in currs:
            if curr.attrib['ID'] == 'R01235':
                usd_curr = float(curr.find('Value').text.replace(',', '.'))
            if curr.attrib['ID'] == 'R01239':
                eur_curr = float(curr.find('Value').text.replace(',', '.'))

    root = etree.parse(opt_price.name).getroot()
    opt_price.close()
    date = timezone.datetime.strptime(root.get('date'), '%Y-%m-%dT%H:%M:%S')
    catalog = {'date': date}

    products_by_id = {}
    offers = root.findall(".//offers/")
    for offer in offers:
        product_dict = {'barcode': offer.find('barcode').text,
                        'name': offer.find('name').text}

        offer_id = offer.find('artikul').text
        if offer_id is not None:
            product_dict['offer_id'] = offer_id
        else:
            product_dict['offer_id'] = offer.get('id')

        if offer.get('available') == 'true':
            product_dict['stock'] = 5
        else:
            product_dict['stock'] = 0

        curr_id = offer.find('currencyId').text
        raw_price = float(offer.find('price').text)
        if curr_id == 'USD':
            product_dict['price'] = round(raw_price * usd_curr, 2)
        elif curr_id == 'EUR':
            product_dict['price'] = round(raw_price * eur_curr, 2)
        elif curr_id == 'RUB':
            product_dict['price'] = raw_price
        products_by_id[offer.get('id')] = product_dict

    root = etree.parse(rrc_price.name).getroot()
    rrc_price.close()
    offers = root.findall(".//offers/")
    for offer in offers:
        if offer.get('id') in products_by_id:
            product_dict = {}
            curr_id = offer.find('currencyId').text
            try:
                raw_price = float(offer.find('price').text)
            except ValueError:
                continue
            if curr_id == 'USD':
                product_dict['rec_price'] = round(raw_price * usd_curr, 2)
            elif curr_id == 'EUR':
                product_dict['rec_price'] = round(raw_price * eur_curr, 2)
            elif curr_id == 'RUB':
                product_dict['rec_price'] = raw_price
            products_by_id[offer.get('id')].update(product_dict)

    catalog['products'] = products_by_id.values()

    return catalog
