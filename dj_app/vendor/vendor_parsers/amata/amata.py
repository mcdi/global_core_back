from vendor.utils.email_getter import get_email_data
from vendor.utils.locale_context_manager import setlocale
import os
import pyexcel
from openpyxl import load_workbook
from vendor.utils.xl_parser import parse_xl
from django.utils import timezone
import tempfile


def amata(*args, **kwargs):
    email_data = get_email_data(sender='n.popova@amatagroup.ru', file_name_filter='опт1 МСК')
    files = email_data.get('files')
    xls_file = files[-1]

    with tempfile.NamedTemporaryFile(suffix='.xlsx') as tmp:
        pyexcel.save_book_as(
            file_name=xls_file,
            dest_file_name=tmp.name)

        book = load_workbook(
            filename=tmp.name,
            read_only=True)
        sheet = book.active

        for row in sheet.iter_rows(min_row=1, max_row=1, min_col=1, max_col=1):
            for cell in row:
                date = cell.value.split('на ')[-1]
                with setlocale('ru_RU.utf8'):
                    date = timezone.datetime.strptime(date, "%d %B %Y г.")

        catalog = {'date': date}

        fields = {
            'offer_id': 'КодСХарактеристикой',
            'price': 'Оптовая 1 (крупные интернет-магазины)',
            'stock': '10',
            'vat': 'НДС',
            'name': 'Номенклатура',
            'barcode': 'Штрихкод',
        }

        def vat_validator(cell_value):
            if cell_value.endswith('%'):
                return int(cell_value[:-1])
            elif cell_value.lower() == 'Без НДС'.lower():
                return 0
            else:
                return int(cell_value)

        def stock_validator(cell_value):
            if cell_value is None:
                return 0
            else:
                return int(cell_value)

        catalog['products'] = parse_xl(tmp.name, fields, fields_rows=(3, 4), not_null_field='offer_id',
                                       validators={'vat': vat_validator, 'stock': stock_validator})
    return catalog
