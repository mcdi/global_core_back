from openpyxl import load_workbook
import pyexcel
from ...utils.email_getter import get_email_data
import os
from django.utils import timezone
from ...utils.locale_context_manager import setlocale


def sibriz(*args, **kwargs):
    email_data = get_email_data(sender='lioncosm@mail.ru', forwarded_by_filter='b2b.ecom@ecomseller.ru')
    files = email_data.get('files')

    dates = []

    files_dict = {}
    for file in files:
        file_name = file.split('/')[-1]
        try:
            int(file_name.split('.')[0])
        except TypeError:
            pass
        except ValueError:
            pass
        else:
            files_dict['stock_file'] = file
        if file_name.startswith('Прайс'):
            files_dict['price_file'] = file
        if file_name.startswith('Остатки') or file_name.startswith('остатки'):
            files_dict['stock_file'] = file

    if 'price_file' not in files_dict:
        email_data = get_email_data(sender='lioncosm@mail.ru', forwarded_by_filter='b2b.ecom@ecomseller.ru',
                                    file_name_filter='Прайс')
        files = email_data.get('files')
        files_dict['price_file'] = files[-1]

    catalog = {}
    stock_dict = {}
    if 'stock_file' in files_dict:
        xls_file = files_dict.pop('stock_file')
        xlsx_file = xls_file + 'x'

        pyexcel.save_book_as(
            file_name=xls_file,
            dest_file_name=xlsx_file)

        book = load_workbook(
            filename=xlsx_file,
            read_only=True)
        sheet = book.active

        for row in sheet.iter_rows(min_row=1, max_row=5, min_col=2, max_col=2):
            for cell in row:
                if 'Период' in cell.value:
                    stock_date = cell.value.split(':')[-1]
                    while stock_date.startswith(' '):
                        stock_date = stock_date[1:]
                    with setlocale('ru_RU.utf8'):
                        dates.append(timezone.datetime.strptime(stock_date, "%d %B %Y г."))

        column_names = {}

        for row in sheet.iter_rows(min_row=7, max_col=3, max_row=9):
            for cell in row:
                if cell.value is None:
                    continue
                if 'остаток' in str(cell.value):
                    column_names['stock'] = cell.column
                if 'Артикул' in str(cell.value):
                    column_names['offer_id'] = cell.column

        for row in sheet.iter_rows(min_row=10, min_col=2):
            product_dict = {}
            current_offer_id = None
            for cell in row:
                if cell.column == column_names['offer_id'] and (str(cell.value)).split('-')[0].isdigit() \
                        and cell.value is not None:
                    product_dict['offer_id'] = str(cell.value)
                    current_offer_id = str(cell.value)
                if cell.column == column_names['stock'] and cell.value is not None:
                    product_dict['stock'] = int(cell.value)
            if current_offer_id is not None:
                stock_dict[current_offer_id] = product_dict

        os.remove(xlsx_file)

    if 'price_file' in files_dict:
        xlsx_file = files_dict.pop('price_file')

        book = load_workbook(
            filename=xlsx_file,
            read_only=True)
        sheet = book.active

        column_names = {}

        price_date = xlsx_file.split(' ')[-1][:-5]
        dates.append(timezone.datetime.strptime(price_date, '%d.%m.%Y'))

        for row in sheet.iter_rows(min_row=9, max_col=12, max_row=9):
            for cell in row:
                if cell.value is None:
                    continue
                if 'ЦЕНА' in str(cell.value):
                    column_names['price'] = cell.column
                if 'АРТИКУЛ' in str(cell.value):
                    column_names['offer_id'] = cell.column
                if 'НАИМЕНОВАНИЕ' in str(cell.value):
                    column_names['name'] = cell.column
                if 'ШТРИХКОД' in str(cell.value):
                    column_names['barcode'] = cell.column

        for row in sheet.iter_rows(min_row=10, max_col=9):
            product_dict = {}
            current_offer_id = None
            for cell in row:
                if cell.column == column_names['offer_id'] and cell.value is not None:
                    product_dict['offer_id'] = str(cell.value)
                    current_offer_id = str(cell.value)
                if cell.column == column_names['price'] and cell.value is not None:
                    product_dict['price'] = float(cell.value)
                if cell.column == column_names['name'] and cell.value is not None:
                    product_dict['name'] = str(cell.value)
                if cell.column == column_names['barcode'] and cell.value is not None:
                    product_dict['barcode'] = str(cell.value)
            if current_offer_id is not None:
                try:
                    stock_dict[current_offer_id].update(product_dict)
                except KeyError:
                    stock_dict[current_offer_id] = product_dict

    for file in files:
        os.remove(file)

    catalog['date'] = max(dates)
    catalog['products'] = stock_dict.values()
    return catalog
