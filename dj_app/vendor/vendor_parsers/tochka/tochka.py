from ...utils.email_getter import get_email_data
from pyunpack import Archive
import os
from ...utils.xl_parser import parse_xl
import tempfile


def tochka(*args, **kwargs):
    email_data = get_email_data(sender='optkniga@inbox.ru', file_name_filter='Прайс')
    files = email_data.get('files')
    archive_file = files[-1]
    with tempfile.TemporaryDirectory() as tmp_dir:
        Archive(archive_file).extractall(tmp_dir)
        xls_file = os.path.join(tmp_dir, '.'.join(archive_file.split('/')[-1].split('.')[:-1]) + '.xls')

    catalog = {'date': email_data.get('date')}

    fields = {
        'offer_id': 'Код',
        'price': 'Цена прайс',
        'stock': 'Свободный остаток',
        'vat': 'НДС',
        'name': 'Номенклатура',
        'barcode': 'EAN',
    }
    catalog['products'] = parse_xl(xls_file, fields, fields_rows=(2, 2), not_null_field='price')
    os.remove(xls_file)

    return catalog
