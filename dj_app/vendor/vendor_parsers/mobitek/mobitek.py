from openpyxl import load_workbook
import pyexcel
from django.utils import timezone
import os
from ...utils.xl_parser import parse_xlsx
from ...utils.email_getter import get_email_data


def mobitek(*args, **kwargs):
    email_data = get_email_data(sender='mariya@armina.ru')
    if len(email_data.get('files')) == 0:
        raise Exception('No files')
    xls_file = email_data.get('files')[-1]
    if not xls_file.endswith('x'):
        xlsx_file = xls_file + 'x'

        pyexcel.save_book_as(
            file_name=xls_file,
            dest_file_name=xlsx_file)

        os.remove(xls_file)
    else:
        xlsx_file = xls_file

    book = load_workbook(
        filename=xlsx_file,
        read_only=True)
    sheet = book.active

    for row in sheet.iter_rows(min_row=7, max_row=7, min_col=2, max_col=2):
        for cell in row:
            date = cell.value.split('на ')[-1]
            date = timezone.datetime.strptime(date, '%d.%m.%Y')

    catalog = {'date': date}

    def name_validator(cell_value):
        return cell_value.strip()

    def stock_validator(cell_value):
        return int(cell_value)

    fields = {
        'offer_id': 'Номенклатура.Код',
        'price': 'Цена',
        'name': 'Ценовая группа/ Номенклатура/ Характеристика номенклатуры',
        'barcode': 'Штрихкод',
        'stock': 'Остаток'
    }
    catalog['products'] = parse_xlsx(xlsx_file, fields, fields_rows=(10, 11), not_null_field='price',
                                     validators={'name': name_validator, 'stock': stock_validator})
    return catalog
