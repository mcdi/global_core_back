import requests
import os
from lxml import etree
from django.utils import timezone
import tempfile


def fhobby(url, *args, **kwargs):
    # url = 'https://fhobby.ru/bitrix/catalog_export/fh_price.php'
    # inn = '7735567310'

    response = requests.get(url)
    assert response.status_code == 200

    with tempfile.NamedTemporaryFile() as f:
        f.write(response.content)
        root = etree.parse(f.name).getroot()

    date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
    catalog = {'date': date}
    products = []
    offers = root.findall(".//offers/")
    for offer in offers:
        product_dict = {
            'name': offer.find('model').text,
            'offer_id': offer.find('vendorCode').text,
            'stock': round(float(offer.find('quantity').text)),
            'vat': 20,
        }
        try:
            product_dict['price'] = float(offer.find('trade_price').text)
        except AttributeError:
            product_dict['price'] = 0

        try:
            product_dict['rec_price'] = float(offer.find('retail_price').text)
        except AttributeError:
            product_dict['rec_price'] = 0

        for param in offer.findall('param'):
            if param.get('name') == 'ШтрихКод':
                product_dict['barcode'] = param.text

        if 'barcode' not in product_dict:
            product_dict['barcode'] = offer.get('id')
        products.append(product_dict)

    catalog['products'] = products
    return catalog
