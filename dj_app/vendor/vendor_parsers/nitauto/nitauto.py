from ...utils.email_getter import get_email_data
from ...utils.xl_parser import parse_xlsx
from openpyxl import load_workbook
from pyexcel import save_book_as
import os
from django.utils import timezone


def nitauto(*args, **kwargs):
    email_data = get_email_data(sender='price@nitauto.ru')
    file = email_data.get('files')[-1]
    date = email_data.get('date')

    try:
        book = load_workbook(
            filename=file,
            read_only=True)
        sheet = book.active
    except KeyError:
        save_book_as(
            file_name=file,
            dest_file_name=file)

        book = load_workbook(
            filename=file,
            read_only=True)
        sheet = book.active

    for row in sheet.iter_rows(min_row=1, max_row=1, min_col=1, max_col=1):
        for cell in row:
            if 'Остатки товаров' in cell.value:
                date = cell.value.split('на ', 1)[-1]
                while date.startswith(' '):
                    date = date[1:]
                date = timezone.datetime.strptime(date, "%d.%m.%Y")

    catalog = {'date': date}

    fields = {
        'offer_id': 'Код поставщика',
        'price': 'Цена, руб.',
        'stock': 'Остаток',
        'name': 'Наименование',
    }

    def stock_validator(cell_value):
        if cell_value == '#N/A':
            return 0
        else:
            return int(cell_value)

    products = parse_xlsx(file, fields, fields_rows=(3, 3), not_null_field='offer_id',
                          validators={'stock': stock_validator})
    if products[-1]['offer_id'] == '#N/A':
        products = products[:-1]

    catalog['products'] = products
    os.remove(file)
    return catalog
