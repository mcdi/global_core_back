from ...utils.email_getter import get_email_data
from lxml import etree
from django.utils import timezone
from html import unescape
import os


def igrotreid(*args, **kwargs):
    email_data = get_email_data(sender='debt@igrotrade.com')
    files = email_data.get('files')
    file = files[-1]

    root = etree.parse(file).getroot()
    date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
    catalog = {'date': date}
    products = []
    product_dict = {}

    offers = root.findall(".//offers/")

    for offer in offers:
        product_dict['offer_id'] = str(offer.attrib['id'])
        for el in offer.getchildren():
            if el.tag == 'price':
                product_dict['price'] = float(el.text.replace(',', '.').replace(u'\xa0', ''))
            elif el.tag == 'model':
                product_dict['name'] = unescape(str(el.text))
            elif el.tag == 'VAT':
                product_dict['vat'] = int(el.text[:-1])
            elif el.tag == 'BarCode':
                product_dict['barcode'] = str(el.text)
            elif el.tag == 'quantrum':
                product_dict['stock'] = int(el.text.replace(u'\xa0', ''))
        # product_dict['rec_price'] = None
        products.append(product_dict)
        product_dict = {}

    catalog['products'] = products
    os.remove(file)
    return catalog
