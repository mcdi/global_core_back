from ...utils.email_getter import get_email_data
import os
from ...utils.xl_parser import parse_xl


def souzplastic(*args, **kwargs):
    # inn = '7720786831'

    email_data = get_email_data(sender='kalyn@souzplastic.ru')
    files = email_data.get('files')
    file = files[-1]
    # file = '/home/pavel/Загрузки/Общий прайс без фото - с остатками.xlsx'

    catalog = {'date': email_data.get('date')}

    fields = {
        'offer_id': 'Код',
        'price': 'Базовая цена',
        'stock': 'Свободный остаток на складах',
        'name': 'Наименование',
        'barcode': 'Штрих-код',
    }

    def price_validator(cell_value):
        return round(float(cell_value) * 0.91, 2)

    catalog['products'] = parse_xl(file, fields, fields_rows=(2, 2), not_null_field='price',
                                   validators={'price': price_validator})
    os.remove(file)

    return catalog
