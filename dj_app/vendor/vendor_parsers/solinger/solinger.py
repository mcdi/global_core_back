from ...utils.email_getter import get_email_data
import os
from ...utils.xl_parser import parse_xl


def solinger(*args, **kwargs):
    # inn = '7743683448'

    email_data = get_email_data(sender='dg@solinger.ru', file_name_filter='Прайс')
    files = email_data.get('files')
    file = files[-1]

    catalog = {'date': email_data.get('date')}

    fields = {
        'offer_id': 'Артикул',
        'price': 'Цена с максимальной скидкой',
        'stock': 'Свободный остаток на складах',
        'name': 'Номенклатура',
    }

    def offer_id_validator(cell_value):
        return {'offer_id': cell_value, 'stock': 10}

    catalog['products'] = parse_xl(file, fields, fields_rows=(3, 3), not_null_field='price',
                                   validators={'offer_id': offer_id_validator})
    os.remove(file)
    return catalog
