import requests
import os
from html import unescape
from django.utils import timezone
from lxml import etree
import tempfile


def rm(url, *args, **kwargs):
    # url = 'https://rm-profiline.online/xml/hannover/client2896/wms5/brand0/?token=$1$sfI11XKV$JL/8WNFETZutmc4sOQy9w1'

    response = requests.get(url)
    assert response.status_code == 200

    with tempfile.NamedTemporaryFile() as f:
        f.write(response.content)

        root = etree.parse(f.name).getroot()
    date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
    catalog = {'date': date}

    offers = root.findall(".//offers/")
    products = []

    for offer in offers:
        product_dict = {}
        for el in offer.getchildren():
            if el.tag == 'price':
                product_dict['price'] = float(el.text)
            if el.tag == 'name':
                product_dict['name'] = unescape(str(el.text))
            if el.tag == 'vendorCode':
                product_dict['offer_id'] = str(el.text)
            if el.tag == 'param':
                if el.attrib['name'] == 'Code':
                    product_dict['barcode'] = str(el.text)
                if el.attrib['name'] == 'WarehouseStock':
                    product_dict['stock'] = int(el.text)
        products.append(product_dict)

    catalog['products'] = products
    return catalog
