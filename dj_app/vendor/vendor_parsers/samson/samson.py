import requests
from typing import List
from django.utils import timezone


def samson(*args, **kwargs):
    api = 'https://api.samsonopt.ru/v1/sku/'
    api_key = '174637cbd4134d3de3690075648e59f3'

    def parse_data(data) -> List[dict]:
        result = []
        for product in data:
            product_dict = {
                'name': product['name'],
                'offer_id': product['vendor_code'],
                'barcode': product['barcode'],
                'vat': product['nds']
            }
            for price in product['price_list']:
                if price['type'] == 'contract':
                    product_dict['price'] = price['value']
                elif price['type'] == 'infiltration':
                    product_dict['rec_price'] = price['value']
            product_dict['stock'] = 0
            for stock in product['stock_list']:
                if stock['type'] == 'total':
                    product_dict['stock'] = stock['value']
                    break
                else:
                    product_dict['stock'] += stock['value']
            result.append(product_dict)
        return result

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
        'Accept': 'application/json'
    }

    params = {
        'api_key': api_key,
        'pagination_count': 10000,
    }

    response = requests.get(api, params=params, headers=headers)
    r_json = response.json()
    next_page = r_json['meta']['pagination'].get('next', None)

    date = timezone.now()
    catalog = {'date': date}

    products = []
    products.extend(parse_data(r_json['data']))
    while next_page is not None:
        response = requests.get(next_page, headers=headers)
        r_json = response.json()
        next_page = r_json['meta']['pagination'].get('next', None)
        products.extend(parse_data(r_json['data']))

    catalog['products'] = products

    return catalog
