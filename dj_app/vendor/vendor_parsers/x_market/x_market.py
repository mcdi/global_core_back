import csv
import requests
import os
from django.utils import timezone
import tempfile


def x_market(url, *args, **kwargs):
    # url = 'https://msk.xmarket.ru/upload/clients/trade_msk.csv'

    response = requests.get(url)
    assert response.status_code == 200
    with tempfile.NamedTemporaryFile() as f:
        f.write(response.content)

        date = timezone.datetime.utcfromtimestamp(os.stat(f.name).st_mtime)
        catalog = {'date': date}

        products = []
        with open(f.name, 'rt', encoding='cp1251') as csv_file:
            reader = csv.DictReader(csv_file, delimiter=';')
            for row in reader:
                try:
                    products.append({'offer_id': row['Артикул'], 'stock': int(row.get('Остаток на складе', 0)),
                                     'price': float(row['Цена оптовая']), 'name': row['Название'],
                                     'barcode': row['Штрихкод']})
                except ValueError:
                    pass

        catalog['products'] = products

    return catalog
