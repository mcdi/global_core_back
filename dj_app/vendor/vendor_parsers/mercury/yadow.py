import tempfile
import zipfile
import requests
from xml.etree import ElementTree as et


def extract_xlsx():
    url = 'https://cloud-api.yandex.net:443/v1/disk/public/resources/download' \
          '?public_key=https://yadi.sk/d/-0TGGj9HRgNYHQ?w=1'
    resp = requests.get(url)
    url = resp.json()['href']
    resp = requests.get(url)
    with tempfile.NamedTemporaryFile('ab') as tmp:
        tmp.write(resp.content)
        tmp.seek(0)
        with zipfile.ZipFile(tmp.name, 'r') as zipf, tempfile.NamedTemporaryFile('w+', suffix='xlsx') as xlsxfile:
            xlsxfile.write(zipf.read('mercury/merc.xml').decode('utf8'))
            xlsxfile.seek(0)
            with open('tmp.xlsx', 'w') as f:
                f.write(xlsxfile.read())
            xlsxfile.seek(0)
            return et.fromstring(xlsxfile.read())


def main():
    xlsx = extract_xlsx()
    nsmap = {  # None: 'urn:schemas-microsoft-com:office:spreadsheet',
        'o': 'urn:schemas-microsoft-com:office:office',
        'x': 'urn:schemas-microsoft-com:office:excel',
        'ss': 'urn:schemas-microsoft-com:office:spreadsheet',
        'html': 'http://www.w3.org/TR/REC-html40'}

    catalog = et.Element('yml_catalog')
    shop = et.SubElement(catalog, 'shop')
    offers = et.SubElement(shop, 'offers')
    rows = xlsx.findall('.//ss:Row', nsmap)
    for row in rows[1:]:
        row = row.findall('.//ss:Data', nsmap)
        offer = et.SubElement(offers, 'offer',
                              attrib={'id': row[0].text.replace(' ', '-').replace('_', '-')})

        et.SubElement(offer, 'price').text = str(row[2].text)
        et.SubElement(offer, 'quantity').text = str(row[-1].text) if row[-1].text.find('.') == -1 else '0'
        et.SubElement(offer, 'name').text = row[1].text
    tree = et.ElementTree(catalog).write('media/feed.xml', encoding='utf-8')


if __name__ == '__main__':
    main()
