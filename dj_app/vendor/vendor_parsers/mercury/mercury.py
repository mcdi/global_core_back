from pyunpack import Archive
import os
from ...utils.parser_1c import parse_1c_from_dir
from .yadow import extract_xlsx
from openpyxl import load_workbook
import tempfile
import requests


def mercury(*args, **kwargs):
    # inn = '6163209120'
    sender = kwargs['sender']
    try:
        vendor = sender.objects.select_related('private_files').get(pk=kwargs['pk'])
    except KeyError:
        vendor = sender.objects.select_related('private_files').get(name='mercury')

    if vendor.private_files.filter(file__icontains='exchange_1c').count() >= 2:
        with tempfile.TemporaryDirectory() as tmp_dir:
            for vendor_file in vendor.private_files.filter(file__icontains='exchange_1c').all().order_by('uploaded_at'):
                file = vendor_file.file
                with tempfile.NamedTemporaryFile(suffix=os.path.basename(file.name)) as tmp_file:
                    resp = requests.get(file.url)
                    tmp_file.write(resp.content)
                    Archive(tmp_file.name).extractall(tmp_dir)
                vendor_file.delete()

            catalog = parse_1c_from_dir(tmp_dir)
        products_1c = catalog['products']
        by_offer_id_1c = {item['offer_id']: item for item in products_1c}

        xlsx = extract_xlsx()
        nsmap = {
            'o': 'urn:schemas-microsoft-com:office:office',
            'x': 'urn:schemas-microsoft-com:office:excel',
            'ss': 'urn:schemas-microsoft-com:office:spreadsheet',
            'html': 'http://www.w3.org/TR/REC-html40'
        }
        products_ya = []
        rows = xlsx.findall('.//ss:Row', nsmap)
        for row in rows[1:]:
            row = row.findall('.//ss:Data', nsmap)
            products_ya.append({
                'offer_id': str(row[0].text),
                'name': str(row[1].text),
                'price': float(row[2].text),
                'vat': int(float(row[4].text) * 100) if row[4].text.find('.') != -1 else int(float(row[3].text) * 100),
                'stock': int(row[-1].text) if row[-1].text.find('.') == -1 else 0,
            })

        book = load_workbook(os.path.join(os.path.dirname(__file__), 'rule.xlsx'), read_only=True, data_only=True)
        sheet = book.active
        rule = [{'offer_id': row[0].value, 'name': row[1].value, 'tag': row[2].value} for row in
                sheet.iter_rows(min_row=2)]

        for product in products_ya:
            try:
                product['stock'] = by_offer_id_1c[product['offer_id']]['stock']
                product['barcode'] = by_offer_id_1c[product['offer_id']]['barcode']
            except KeyError:
                for product_1c in products_1c:
                    if product['name'] in product_1c['name']:
                        for r in rule:
                            if r['tag'] in product['name']:
                                product['stock'] = by_offer_id_1c[r['offer_id']]['stock']
                                product['barcode'] = by_offer_id_1c[r['offer_id']]['barcode']
                            else:
                                product['stock'] = 0

        catalog['products'] = products_ya
        return catalog
