from ...utils.xl_parser import parse_xl
from ...utils.email_getter import get_email_data
import os


def avtoalians(*args, **kwargs):
    email_data = get_email_data(sender='script@autoopt.ru')
    files = email_data.get('files')
    file = files[-1]

    catalog = {'date': email_data.get('date')}

    fields = {
        'offer_id': 'Артикул',
        'price': 'Цена',
        'stock': 'Остаток Варшавка',
        'name': 'Наименование товара',
        'barcode': 'Код товара',
    }

    def stock_validator(cell_value):
        if cell_value is None:
            return {'stock': 0, 'vat': 20}
        else:
            return {'stock': int(cell_value), 'vat': 20}

    catalog['products'] = parse_xl(file, fields, fields_rows=(1, 1), not_null_field='offer_id',
                                   validators={'stock': stock_validator})
    return catalog
