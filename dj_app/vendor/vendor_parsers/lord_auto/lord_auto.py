import requests
import os
from lxml import etree
from django.utils import timezone
import tempfile


def lord_auto(url, *args, **kwargs):
    # url = 'http://ftp113281.hostfx.ru/StockAli.xml'
    # inn = '7727819483'

    response = requests.get(url)
    assert response.status_code == 200

    with tempfile.NamedTemporaryFile() as f:
        f.write(response.content)
        root = etree.parse(f.name).getroot()

    date = timezone.datetime.strptime(root.get('Дата'), '%d.%m.%Y')
    catalog = {'date': date}

    products = []
    offers = root.findall(".Товар")

    for offer in offers:
        product_dict = {
            'offer_id': offer.get('Артикул'),
            'price': float(offer.get('Цена')),
            'stock': round(float(offer.get('Остаток'))),
        }
        products.append(product_dict)

    catalog['products'] = products
    return catalog
