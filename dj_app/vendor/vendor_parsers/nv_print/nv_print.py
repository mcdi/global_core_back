import requests
from lxml import etree
import os
from django.utils import timezone
import tempfile


def nv_print(*args, **kwargs):
    resp = requests.get(
        'http://r.kazakov%40ecomseller.ru:m7zqf4bw@api.nvprint.ru/api/hs/getprice/643/9731045388/772301001',
        params={'format': 'xml'})
    assert resp.status_code == 200

    with tempfile.NamedTemporaryFile() as f:
        f.write(resp.content)
        root = etree.parse(f.name).getroot()

    date = timezone.datetime.strptime(root.attrib['ДатаФормирования'], '%Y-%m-%dT%H:%M:%S')
    catalog = {'date': date}
    products = []
    offers = root.findall(".//Товары/")
    for offer in offers:
        product_dict = {
            'offer_id': offer.find('Артикул').text,
            'barcode': offer.find('Код').text,
            'name': offer.find('Номенклатура').text,
        }
        price = offer.find('.УсловияПродаж/Договор/Цена').text
        stock = offer.find('.УсловияПродаж/Договор/Наличие').get('Количество')
        if price is not None:
            product_dict['price'] = float(price)
        else:
            continue
        product_dict['stock'] = int(stock) if stock != '' else 0
        products.append(product_dict)
    catalog['products'] = products
    return catalog
