from ...utils.email_getter import get_email_data
import os
from ...utils.xl_parser import parse_xl


def pgs_olimp(*args, **kwargs):
    email_data = get_email_data(sender='ikarpacheva@mail.ru')
    files = email_data.get('files')
    xls_file = files[-1]

    catalog = {'date': email_data.get('date')}

    fields = {
        'offer_id': 'Артикул',
        'price': 'Цена',
        'stock': 'Свободный остаток',
        'name': 'Номенклатура',
    }
    catalog['products'] = parse_xl(xls_file, fields, fields_rows=(1, 2), not_null_field='price')
    os.remove(xls_file)

    return catalog
