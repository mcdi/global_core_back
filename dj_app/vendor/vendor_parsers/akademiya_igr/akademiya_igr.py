import requests
from lxml import etree
from django.utils import timezone
from html import unescape
import tempfile


def akademiya_igr(url, *args, **kwargs):
    response = requests.get(url)

    with tempfile.NamedTemporaryFile('ab') as tmp:
        tmp.write(response.content)
        tmp.seek(0)
        root = etree.parse(tmp.name).getroot()

    date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
    date = timezone.make_aware(date)
    catalog = {'date': date}
    products = []
    product_dict = {}

    offers = root.findall(".//offers/")

    for offer in offers:
        product_dict['name'] = unescape(str(offer.find('name').text))
        product_dict['price'] = float(offer.find('price').text)
        product_dict['stock'] = unescape(str(offer.find('stock').text))
        product_dict['vat'] = int(offer.find('nds').text)
        product_dict['barcode'] = unescape(str(offer.find('ShtrihKod').text))
        product_dict['offer_id'] = unescape(str(offer.find('vendorCode').text))

        # product_dict['rec_price'] = None
        products.append(product_dict)
        product_dict = {}

    catalog['products'] = products
    return catalog
