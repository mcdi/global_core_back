import requests
from django.utils import timezone


def sila_vodi(*args, **kwargs):
    url = 'https://account.wasserkraft.ru/rest/products'

    # inn = '7713643335'

    params = {
        'limit': 1
    }

    headers = {
        'Authorization': 'Static w5PEoMbO0NG0p9nTs5u%2FxqLCt9G5tavC2t7K4K6qk8fIlMSatJqs29e5zam4jJjFtJa6oZmTRFZWWnN4TGhsNTEwMg%3D%3D'
    }

    resp = requests.get(url, params=params, headers=headers)
    assert resp.status_code == 200

    params['limit'] = resp.json()['total']

    resp = requests.get(url, params=params, headers=headers)
    assert resp.status_code == 200

    json = resp.json()
    cur_time = timezone.now()
    catalog = {'date': cur_time}

    products = []
    for item in json['results']:
        product = {
            'price': float(item['Price']),
            'stock': int(item['Quantity']),
            'offer_id': str(item['Article']),
            'name': str(item['Name']),
        }
        try:
            ean_code = item['EAN_Code']
        except KeyError:
            try:
                ean_code = item['EAN Code']
            except KeyError:
                ean_code = ''

        if ean_code != '':
            product['barcode'] = str(ean_code)
        try:
            product['rec_price'] = float(item['MRRC_Price'])
        except KeyError:
            product['rec_price'] = float(item['MRRC Price'])

        products.append(product)

    catalog['products'] = products

    return catalog
