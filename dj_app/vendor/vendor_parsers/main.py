from .bm_restart import bm_restart
from .dzhamper import dzhamper
from .korea_treid import korea_treid
from .tochka import tochka
from .dzhin import dzhin
from .sibriz import sibriz
from .tdk import tdk
from .igrotreid import igrotreid
from .akademiya_igr import akademiya_igr
from .ns_opt import ns_opt
from .schaste import schaste
from .rm import rm
from .sportex import sportex
from .amata import amata
from .x_market import x_market
from .samson import samson
from .avtoalians import avtoalians
from .gela import gela
from .mobitek import mobitek
from .reikanen import reikanen
from .nitauto import nitauto
from .kolesa_darom import kolesa_darom
from .instrument_ru import instrument_ru
from .kolyaski_optom import kolyaski_optom
from .kurazh_mebel import kurazh_mebel
from .forward_bike import forward_bike
from .lord_auto import lord_auto
from .decathlon import decathlon
from .driada import driada
from .neotren import neotren
from .own_warehouse import own_warehouse
from .kettler import kettler
from .nv_print import nv_print
from .mercury import mercury
from .fhobby import fhobby
from .solinger import solinger
from .sila_vodi import sila_vodi
from .nordwest import nordwest
# non-active
from .pgs_olimp import pgs_olimp
from .vse_instrumenti import vse_instrumenti
from .souzplastic import souzplastic

VENDORS_FUNCTIONS = {
    'bm_restart': bm_restart,
    'dzhamper': dzhamper,
    'korea_treid': korea_treid,
    'tochka': tochka,
    'sibriz': sibriz,
    'dzhin': dzhin,
    'tdk': tdk,
    'igrotreid': igrotreid,
    'akademiya_igr': akademiya_igr,
    'ns_opt': ns_opt,
    'schaste': schaste,
    'rm': rm,
    'sportex': sportex,
    'amata': amata,
    'x_market': x_market,
    'mobitek': mobitek,
    'pgs_olimp': pgs_olimp,
    'vse_instrumenti': vse_instrumenti,
    'samson': samson,
    'avtoalians': avtoalians,
    'gela': gela,
    'reikanen': reikanen,
    'nitauto': nitauto,
    'kolesa_darom_central': kolesa_darom,
    'kolesa_darom_msk': kolesa_darom,
    'kolesa_darom_beru': kolesa_darom,
    'instrument_ru': instrument_ru,
    'kolyaski_optom': kolyaski_optom,
    'kurazh_mebel': kurazh_mebel,
    'forward_bike': forward_bike,
    'lord_auto': lord_auto,
    'decathlon': decathlon,
    'driada': driada,
    'neotren': neotren,
    'own_warehouse': own_warehouse,
    'kettler': kettler,
    'nv_print': nv_print,
    'mercury': mercury,
    'fhobby': fhobby,
    'souzplastic': souzplastic,
    'solinger': solinger,
    'sila_vodi': sila_vodi,
    'nordwest': nordwest,
}


def get_feed_data(vendor_name, *args, **kwargs):
    return VENDORS_FUNCTIONS[vendor_name](*args, **kwargs)
