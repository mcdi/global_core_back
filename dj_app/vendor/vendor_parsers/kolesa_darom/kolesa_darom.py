from lxml import etree
import requests
from django.utils import timezone
import os
import tempfile

TELEGRAM_BOT_KEY = '1453653051:AAHv8G-JBkmwHRT2XIBQiA1l6nDOU_1J7ZY'
TG_BOT_API_URL = f'https://api.telegram.org/bot{TELEGRAM_BOT_KEY}/'
CHAT_ID = '-1001485934021'


class EmptyFeedException(Exception):
    def __init__(self):
        super().__init__('Feed is empty')


def tg_bot_send_message(message: str, chat_id: str):
    url = TG_BOT_API_URL + 'sendMessage'
    requests.post(url=url, params={"chat_id": chat_id, "text": message})


def kolesa_darom(url, *args, **kwargs):
    try:

        # url = 'https://feed-new.kolesa-darom.ru/feed/live/moskva/ozon_central_wh.xml'
        # url = 'https://feed-new.kolesa-darom.ru/feed/live/moskva/ozon_msk.xml'
        # url = 'https://feed-new.kolesa-darom.ru/feed/live/moskva/beru_msk.xml'
        # inn = '771770396501'

        warehouse = url.split('/')[-1][:-4]
        stock_id = '45' if 'central' in warehouse or 'beru' in warehouse else '43'
        name_field = 'name' if 'beru' in url else 'model'

        response = requests.get(url)
        assert response.status_code == 200

        with tempfile.NamedTemporaryFile() as f:
            f.write(response.content)
            root = etree.parse(f.name).getroot()

        date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
        catalog = {'date': date}
        products = []
        offers = root.findall(".//offers/")
        if len(offers) < 50:
            raise EmptyFeedException
        for offer in offers:
            product_dict = {
                'offer_id': offer.get('id'),
                'price': float(offer.find('price').text),
                'name': offer.find(name_field).text
            }
            for outlet in offer.findall('.//outlet'):
                if outlet.get('id') == stock_id:
                    product_dict['stock'] = int(outlet.get('instock'))
                else:
                    product_dict['stock'] = 0
            # product_dict['product_id'] = None
            # product_dict['vat'] = None
            # product_dict['rec_price'] = None
            products.append(product_dict)

        catalog['products'] = products
        return catalog
    except EmptyFeedException:
        tg_bot_send_message(f'Feed on {url} is empty', CHAT_ID)
        raise
    except AssertionError:
        tg_bot_send_message(f"Can't get feed on {url}", CHAT_ID)
        raise
