import requests
import os
from lxml import etree
from django.utils import timezone
import tempfile


def forward_bike(url, *args, **kwargs):
    # url = 'https://forward.bike/bitrix/catalog_export/export_IVL.xml'
    # inn = '301504878972'

    response = requests.get(url)
    assert response.status_code == 200

    with tempfile.NamedTemporaryFile() as f:
        f.write(response.content)
        root = etree.parse(f.name).getroot()

    date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M:%S')
    catalog = {'date': date}

    products = []
    offers = root.findall(".//offers/")

    for offer in offers:
        product_dict = {
            'price': float(offer.find('price').text),
            'name': offer.find('model').text,
            'offer_id': offer.find('vendorCode').text,
        }
        barcode = offer.find('barcode')
        if barcode is not None:
            product_dict['barcode'] = barcode.text
        for param in offer.findall('./param/'):
            if param.get('name') == 'Доступное количество':
                product_dict['stock'] = param.text
        products.append(product_dict)

    catalog['products'] = products
    return catalog
