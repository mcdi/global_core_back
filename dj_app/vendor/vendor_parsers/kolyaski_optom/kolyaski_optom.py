import requests
from lxml import etree
from django.utils import timezone
import tempfile


def kolyaski_optom(url, *args, **kwargs):
    # url = 'https://vroznicu.kolyaskyoptom.ru/index.php?route=feed/xmlpriceozonall'
    # inn = '5027277440'

    response = requests.get(url)
    assert response.status_code == 200

    with tempfile.NamedTemporaryFile(suffix='.xml') as f:
        f.write(response.content)
        root = etree.parse(f.name).getroot()

    date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
    catalog = {'date': date}
    products = []
    offers = root.findall(".//offers/")
    for offer in offers:
        product_dict = {
            'price': float(offer.find('price').text),
            'name': offer.find('name').text,
            'offer_id': offer.find('vendorCode').text,
            'barcode': offer.find('barcode').text,
            'stock': int(offer.find('quantity').text),
        }
        products.append(product_dict)

    catalog['products'] = products
    return catalog
