import os
from ...utils import parse_xl
import tempfile
import requests
from lxml import etree
from django.utils import timezone


def nordwest(*args, **kwargs):
    stock_url = kwargs.get('stock_url', 'https://remecoclub.ru/xml/siteremeco.xml')
    price_file = kwargs.get('price_file', os.path.join(os.path.dirname(__file__), 'price.xlsx'))

    inn = '7716924655'

    # fields = {
    #     'offer_id': 'Артикул',
    #     'price': 'Цена',
    #     'name': 'Наименование',
    #     'barcode': 'Штрихкод',
    # }
    # products = parse_xl(price_file, fields, fields_rows=(1, 1), not_null_field='price')

    with tempfile.NamedTemporaryFile() as tmp:
        resp = requests.get(stock_url)
        tmp.write(resp.content)
        tmp.seek(0)

        root = etree.parse(tmp).getroot()

        date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
        catalog = {'date': date}
        products = []
        offers = root.findall(".//offers/")

        for offer in offers:
            product_dict = {
                'offer_id': str(offer.find('artikul').text),
                'price': float(offer.find('price').text),
                'name': str(offer.find('name').text),
                'barcode': str(offer.find('barcode').text),
            }

            stock = offer.find('availability').text
            if '-' in stock:
                stock = stock.split('-')[0]
            stock = stock.strip('>-<')
            product_dict['stock'] = int(stock)

            products.append(product_dict)

        catalog['products'] = products
    return catalog
