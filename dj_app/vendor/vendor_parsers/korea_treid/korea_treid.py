from ...utils.email_getter import get_email_data
import os
from ...utils.xl_parser import parse_xl


def korea_treid(*args, **kwargs):
    email_data = get_email_data(sender='info@koreatrade.ru', content_type_filter='html')
    files = email_data.get('files')
    xls_file = files[-1]

    catalog = {'date': email_data.get('date')}

    def stock_validator(cell_value):
        try:
            return int(cell_value)
        except ValueError:
            if str(cell_value) == '> 300':
                return 300
            else:
                return 0

    fields = {
        'offer_id': 'Артикул',
        'price': ' от  15 т.р.',
        'stock': 'Остаток',
        'name': 'Номенклатура',
        'barcode': 'Штрихкод',
    }
    catalog['products'] = parse_xl(xls_file, fields, fields_rows=(4, 4), not_null_field='name',
                                   validators={'stock': stock_validator})
    os.remove(xls_file)
    return catalog
