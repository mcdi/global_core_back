import urllib.request
import os
from ...utils.xl_parser import parse_xlsx
from openpyxl import load_workbook
from django.utils import timezone
import tempfile


def driada(*args, **kwargs):
    url = 'http://driada-sport.ru/data/files/driada-price.xlsx'
    # inn = '7724384690'

    response = urllib.request.urlopen(url)
    content = response.read()

    with tempfile.NamedTemporaryFile(suffix='.xlsx') as f:
        f.write(content)

        book = load_workbook(
            filename=f.name,
            read_only=True)
        sheet = book.active

        for row in sheet.iter_rows(min_row=2, max_row=2, min_col=1, max_col=1):
            for cell in row:
                date = cell.value
                date = timezone.datetime.strptime(date, '%d.%m.%Y  %H:%M')

        catalog = {'date': date}

        fields = {
            'offer_id': 'Артикул',
            'price': 'Розничные',
            'price1': 'Оптовые',
            'price2': 'Оптовые -5%',
            'stock': 'Склад Бирюлево',
            'name': 'Наименование',
        }

        def stock_validator(cell_value):
            if cell_value is None:
                return 0
            elif cell_value == 'более 10':
                return 11
            else:
                return int(cell_value)

        catalog['products'] = parse_xlsx(f.name, fields, fields_rows=(11, 11), not_null_field='price',
                                         validators={'stock': stock_validator})
    return catalog
