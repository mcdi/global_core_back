from openpyxl import load_workbook
import pyexcel
from ...utils.email_getter import get_email_data
import os
from django.utils import timezone
from ...utils.xl_parser import parse_xlsx


# TODO: decide how to resolve many prices
def dzhin(*args, **kwargs):
    email_data = get_email_data(sender=['kubyshkina@sales.gratwest.ru', 'dubyaga@sales.gratwest.ru'],
                                file_name_filter='Еком')
    files = email_data.get('files')
    file = files[-1]

    if file.endswith('.xls'):
        xls_file = file
        xlsx_file = xls_file + 'x'

        pyexcel.save_book_as(
            file_name=xls_file,
            dest_file_name=xlsx_file)

        os.remove(xls_file)
    else:
        xlsx_file = file

    book = load_workbook(
        filename=xlsx_file,
        read_only=True)
    sheet = book.active

    catalog = {}
    for row in sheet.iter_rows(min_row=11, max_row=11, min_col=1, max_col=1):
        for cell in row:
            if 'Дата отчета' in cell.value:
                date = cell.value.split(':', 1)[-1]
                while date.startswith(' '):
                    date = date[1:]
                date = timezone.datetime.strptime(date, "%d.%m.%Y %H:%M:%S")
                catalog['date'] = date

    def vat_validator(cell_value):
        if cell_value.endswith('%'):
            return int(cell_value[:-1])
        else:
            return int(cell_value)

    def stock_validator(cell_value):
        if cell_value is None:
            return 0
        else:
            return int(cell_value)

    fields = {
        'offer_id': 'Артикул',
        'price': 'Итоговая цена',
        'price1': 'Итоговая цена без НДС',
        'stock': 'Свободный остаток',
        'vat': 'Ставка НДС',
        'name': 'Номенклатура',
        'barcode': 'Штрих код',
    }
    catalog['products'] = parse_xlsx(xlsx_file, fields, fields_rows=(13, 13), not_null_field='price',
                                     validators={'vat': vat_validator, 'stock': stock_validator})
    os.remove(xlsx_file)
    return catalog
