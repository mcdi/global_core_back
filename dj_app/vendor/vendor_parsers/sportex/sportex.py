import tempfile

import requests
from django.utils import timezone
from lxml import etree


def sportex(url, *args, **kwargs):
    # url = 'http://www.sportex-msk.ru/download/sellecom-F4ahN74.xml'

    response = requests.get(url)

    with tempfile.NamedTemporaryFile() as f:
        f.write(response.content)
        root = etree.parse(f.name).getroot()

    date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
    catalog = {'date': date}

    offers = root.findall(".//offers/")
    products = []

    for offer in offers:
        product_dict = {'barcode': str(offer.attrib['id'])}
        for el in offer.getchildren():
            if el.tag == 'vendorCode':
                product_dict['offer_id'] = str(el.text)
            if el.tag == 'name':
                product_dict['name'] = str(el.text)
            if el.tag == 'quantity':
                product_dict['stock'] = int(el.text)
            if el.tag == 'price':
                product_dict['price'] = round(float(el.text.replace(',', '.')) * 0.9, 2)
            if el.tag == 'param':
                if el.attrib['name'] == 'РРЦ':
                    float(el.text.replace(',', '.'))
        products.append(product_dict)

    catalog['products'] = products
    return catalog
