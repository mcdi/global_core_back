import requests
from lxml import etree
from django.utils import timezone
from html import unescape
import tempfile


def dzhamper(url, *args, **kwargs):
    response = requests.get(url)

    with tempfile.NamedTemporaryFile(suffix='.xml') as f:
        f.write(response.content)

        root = etree.parse(f.name).getroot()
    date = timezone.datetime.strptime(root.attrib['date'], '%Y-%m-%d %H:%M')
    catalog = {'date': date}
    products = []
    offers = root.findall(".//offers/")

    for offer in offers:
        product_dict = {'barcode': str(offer.attrib['id']), 'stock': int(offer.attrib['available'])}
        for el in offer.getchildren():
            if el.tag == 'price':
                product_dict['price'] = float(el.text)
            elif el.tag == 'name':
                product_dict['name'] = unescape(str(el.text))
            elif el.tag == 'vendorCode':
                product_dict['offer_id'] = str(el.text)
        # product_dict['product_id'] = None
        # product_dict['vat'] = None
        # product_dict['rec_price'] = None
        products.append(product_dict)

    catalog['products'] = products
    return catalog
