from celery import shared_task
from .models import Vendor


@shared_task
def refresh_vendor(vendor_id: int) -> None:
    vendor = Vendor.objects.get(id=vendor_id)
    if vendor.is_refresh_time and not vendor.is_refreshing:
        vendor.refresh_products()


@shared_task
def refresh_vendors() -> None:
    for vendor in Vendor.objects.all():
        if vendor.is_refresh_time and not vendor.is_refreshing:
            refresh_vendor.delay(vendor.id)
