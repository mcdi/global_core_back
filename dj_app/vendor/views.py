import os

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions
from rest_framework import status
from rest_framework import viewsets, permissions
from rest_framework.decorators import action

from .authentication import VendorBasicAuthentication
from .models import Vendor, VendorPrefix, VendorProduct
from .parsers import CustomFileUploadParser
from .serializers.vendor import VendorSerializer
from .serializers.vendor_prefix import VendorPrefixSerializer
from .serializers.vendor_product import VendorProductSerializer
from .tasks import refresh_vendor


class VendorViewSet(viewsets.ModelViewSet):
    """
    # Vendor
    ---
    """
    permission_classes = [permissions.AllowAny]
    serializer_class = VendorSerializer

    def get_queryset(self):
        return Vendor.objects.all()

    @action(methods=['GET', 'POST'], detail=True, authentication_classes=[VendorBasicAuthentication],
            parser_classes=[CustomFileUploadParser])
    def exchange_1c(self, request, pk=None):
        rq_vendor = request.user
        pk_vendor = get_object_or_404(self.get_queryset(), pk=pk)
        if rq_vendor != pk_vendor:
            raise exceptions.PermissionDenied(_("You don't have permission for this vendor"))
        else:
            vendor = pk_vendor

        if request.method == 'GET':
            type_ = request.query_params.get('type', None)
            mode = request.query_params.get('mode', None)

            if type_ == 'catalog' and mode == 'checkauth':
                return HttpResponse('success\ntoken\nslmf@34g', status=status.HTTP_200_OK)
            elif type_ == 'catalog' and mode == 'init':
                return HttpResponse('zip=yes\nfile_limit=104857600', status=status.HTTP_200_OK)
            elif type_ == 'catalog' and mode == 'import':
                return HttpResponse('success', status=status.HTTP_200_OK)
            else:
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        if request.method == 'POST':
            type_ = request.query_params.get('type', None)
            mode = request.query_params.get('mode', None)
            filename = request.query_params.get('filename', None)

            if type_ == 'catalog' and mode == 'file':
                file = request.data['file']
                file._name = os.path.join('exchange_1c', file.name)
                vendor.private_files.create(file=file)

                if vendor.private_files.filter(file__icontains='exchange_1c').count() >= 2:
                    refresh_vendor.delay(vendor.id)

                return HttpResponse('success', status=status.HTTP_201_CREATED)
            else:
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


class VendorPrefixViewSet(viewsets.ModelViewSet):
    """
    # VendorPrefix
    ---
    """
    permission_classes = [permissions.AllowAny]
    serializer_class = VendorPrefixSerializer

    def get_queryset(self):
        return VendorPrefix.objects.all()


class VendorProductViewSet(viewsets.ModelViewSet):
    """
    # VendorProduct
    ---
    """
    permission_classes = [permissions.AllowAny]
    serializer_class = VendorProductSerializer

    def get_queryset(self):
        return VendorProduct.objects.all()
