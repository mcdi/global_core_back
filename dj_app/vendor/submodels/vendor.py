import traceback
from concurrent.futures import ThreadPoolExecutor
from typing import List

from django.db import models
from django.db.transaction import atomic
from django.utils import timezone

from ..vendor_parsers.main import get_feed_data

REFRESH_COOLDOWN = 10


class Vendor(models.Model):
    """

    """

    # Custom fields
    name = models.CharField(max_length=1024, verbose_name="Vendor name")

    # Vendors base fields
    feed_source = models.CharField(max_length=1024, verbose_name="Vendor's feed source", null=True, blank=True)
    inn = models.CharField(max_length=12, null=True, blank=True)
    refresh_period = models.IntegerField(default=60, verbose_name='Time to wait before next refresh in minutes')
    refresh_date = models.DateTimeField(default=timezone.datetime(1, 1, 1, tzinfo=timezone.utc),
                                        verbose_name='Vendor refresh time')
    feed_date = models.DateTimeField(default=timezone.datetime(1, 1, 1, tzinfo=timezone.utc),
                                     verbose_name="Vendor's feed update time")

    def get_feed(self) -> dict:
        return get_feed_data(self.name, self.feed_source, pk=self.pk, sender=self.__class__)

    # TODO: refactor for bulk_create()/bulk_update() methods usage
    @atomic
    def save_products(self, products) -> None:
        for product in products:
            offer_id = product.pop('offer_id', None)
            barcode = product.pop('barcode', None)
            if offer_id is not None:
                if barcode is not None:
                    self.products.update_or_create(offer_id=offer_id, barcode=barcode, defaults=product)
                else:
                    self.products.update_or_create(offer_id=offer_id, defaults=product)

    def refresh_products(self, force: bool = False) -> None:
        report = self.reports.create()
        try:
            feed = self.get_feed()
            products = feed.get('products', list())
            feed_date = feed.get('date', timezone.datetime(1, 1, 1, tzinfo=timezone.utc))
            if feed_date.tzinfo is None:
                feed_date = feed_date.replace(tzinfo=timezone.get_current_timezone())

            if self.feed_date < feed_date or force:
                self.save_products(products)
                self.feed_date = feed_date
                self.refresh_date = timezone.now()
                self.save()
        except Exception as ex:
            report.is_success = False
            report.comment = traceback.format_exc()
            raise
        finally:
            report.end_time = timezone.now()
            report.save()

    @property
    def is_refresh_time(self) -> bool:
        last_report = self.reports.order_by('-start_time').first()
        if last_report is None or last_report.is_success:
            reports_depend_status = True
        else:
            reports_depend_status = (
                    timezone.localtime() - timezone.timedelta(minutes=REFRESH_COOLDOWN) > last_report.start_time)
        period_depend_status = timezone.localtime() - timezone.timedelta(
            minutes=self.refresh_period) > self.refresh_date
        return reports_depend_status and period_depend_status

    @property
    def is_refreshing(self) -> bool:
        last_report = self.reports.order_by('-start_time').first()
        return last_report.end_time is None if last_report else False

    # Refreshing on one machine (without celery workers)
    @classmethod
    def refresh(cls) -> None:
        with ThreadPoolExecutor(max_workers=2) as executor:
            futures = [executor.submit(vendor.refresh_products()) for vendor
                       in cls.objects.all() if vendor.is_refresh_time()]

    @classmethod
    def get_prefixes(cls) -> List[str]:
        prefixes = set()
        for queryset in (vendor.prefixes.values_list('name', flat=True) for vendor in
                         cls.objects.prefetch_related('prefixes').all()):
            prefixes.update(queryset)
        return sorted(prefixes)
