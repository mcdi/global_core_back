from django.db import models
from .vendor import Vendor


class VendorPrefix(models.Model):
    """

    """
    # Relations
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE, related_name='prefixes')
    name = models.CharField(max_length=1024, verbose_name="Vendor prefix")
