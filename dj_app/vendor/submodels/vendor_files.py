from django.db import models
from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver

from core.storage_backends import PublicMediaStorage, PrivateMediaStorage
from .vendor import Vendor


class VendorPublicFile(models.Model):
    uploaded_at = models.DateTimeField(auto_now_add=True)
    file = models.FileField(storage=PublicMediaStorage)
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE, verbose_name='Vendor public files',
                               related_name='public_files')


class VendorPrivateFile(models.Model):
    uploaded_at = models.DateTimeField(auto_now_add=True)
    file = models.FileField(storage=PrivateMediaStorage)
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE, verbose_name='Vendor private files',
                               related_name='private_files')


@receiver(pre_save, sender=VendorPublicFile)
@receiver(pre_save, sender=VendorPrivateFile)
def auto_delete_file_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        old_file = sender.objects.get(pk=instance.pk).file
    except sender.DoesNotExist:
        return False

    new_file = instance.file
    if not old_file == new_file:
        old_file.delete(save=False)


@receiver(post_delete, sender=VendorPublicFile)
@receiver(post_delete, sender=VendorPrivateFile)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.file:
        instance.file.delete(save=False)
