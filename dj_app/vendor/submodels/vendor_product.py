from django.db import models
from .vendor import Vendor


class VendorProduct(models.Model):
    """

    """
    # Relations
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE, verbose_name="Parent vendor", related_name='products')

    # Fields
    offer_id = models.CharField(max_length=1024, verbose_name="Vendor code")
    stock = models.IntegerField(default=0, verbose_name="Quantity balance")
    price = models.FloatField(default=0, verbose_name="Product price")

    barcode = models.CharField(max_length=1024, verbose_name="Barcode", null=True, blank=True)
    rrp = models.FloatField(verbose_name="Recommended retail price", null=True, blank=True)
    vat = models.FloatField(verbose_name="Value added tax", null=True, blank=True)
    name = models.CharField(max_length=1024, verbose_name='Product name', null=True, blank=True)
