from django.db import models
from django.utils import timezone
from .vendor import Vendor


class VendorRefreshReport(models.Model):
    """

    """

    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE, related_name='reports')
    start_time = models.DateTimeField(default=timezone.now, verbose_name='Refresh process begin time')
    end_time = models.DateTimeField(null=True, verbose_name='Refresh process end time')
    is_success = models.BooleanField(default=True, verbose_name='Report success status')
    comment = models.TextField(default='', max_length=1024, verbose_name='Report comment')
