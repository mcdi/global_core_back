# Generated by Django 3.1.3 on 2020-12-14 10:24

import core.storage_backends
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vendor', '0007_auto_20201204_1806'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vendor',
            name='api_id',
        ),
        migrations.CreateModel(
            name='VendorPublicFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uploaded_at', models.DateTimeField(auto_now_add=True)),
                ('file', models.FileField(storage=core.storage_backends.PublicMediaStorage, upload_to='')),
                ('vendor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='public_files', to='vendor.vendor', verbose_name='Vendor public files')),
            ],
        ),
        migrations.CreateModel(
            name='VendorPrivateFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uploaded_at', models.DateTimeField(auto_now_add=True)),
                ('file', models.FileField(storage=core.storage_backends.PrivateMediaStorage, upload_to='')),
                ('vendor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='private_files', to='vendor.vendor', verbose_name='Vendor private files')),
            ],
        ),
    ]
