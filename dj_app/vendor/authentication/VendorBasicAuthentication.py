import base64

from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions
from rest_framework.authentication import BasicAuthentication

from vendor.models import Vendor


class VendorBasicAuthentication(BasicAuthentication):
    def authenticate_credentials(self, userid, password, request=None):
        try:
            pass_decoded = base64.b64decode(password).decode('utf-8').partition('::')
        except UnicodeDecodeError:
            pass_decoded = base64.b64decode(password).decode('latin-1').partition('::')
        vendor_id, vendor_name = pass_decoded[0], pass_decoded[2]

        if userid != vendor_name:
            raise exceptions.AuthenticationFailed(_('Invalid username/password.'))

        try:
            vendor = Vendor.objects.get(id=vendor_id)
        except (Vendor.MultipleObjectsReturned, Vendor.DoesNotExist):
            raise exceptions.AuthenticationFailed(_('Vendor not exist'))

        return vendor, None
