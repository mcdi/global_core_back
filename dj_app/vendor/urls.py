from rest_framework.routers import DefaultRouter
from .views import VendorViewSet, VendorPrefixViewSet, VendorProductViewSet

router = DefaultRouter()
router.register('product', VendorProductViewSet, basename='vendor')
router.register('prefix', VendorPrefixViewSet, basename='vendor')
router.register('', VendorViewSet, basename='vendor')

urlpatterns = router.urls
