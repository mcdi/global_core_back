from rest_framework.serializers import ModelSerializer
from ..models import VendorPrefix


class VendorPrefixSerializer(ModelSerializer):
    class Meta:
        model = VendorPrefix
        fields = [
            "id",
            "vendor",
            "name"
        ]
