from rest_framework.serializers import ModelSerializer
from ..models import VendorProduct


class VendorProductSerializer(ModelSerializer):
    class Meta:
        model = VendorProduct
        fields = [
            "id",
            "vendor",
            "offer_id",
            "stock",
            "price",
            "barcode",
            "rrp",
            "vat"
        ]
